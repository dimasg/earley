# -*- coding: utf-8 -*-

# Author: Dimas Gilang
# NIM:    23515027

# input
Grammar = [
    ('S', ('NP', 'VP',)),
    ('S', ('Aux', 'NP', 'VP',)),
    ('S', ('VP',)),
    ('NP', ('Det', 'Nominal',)),
    ('Nominal', ('Noun',)),
    ('Nominal', ('Noun', 'Nominal',)),
    ('NP', ('ProperNoun',)),
    ('VP', ('Verb',)),
    ('VP', ('Verb', 'NP',)),
    ('Det', (['that', 'this', 'a'],)),
    ('Noun', (['book', 'flight', 'meal', 'money'],)),
    ('Verb', (['book', 'include', 'prefer'],)),
    ('Aux', (['does'],)),
    ('Prep', (['from', 'to', 'on'],)),
    ('ProperNoun', (['Houston', 'TWA'],)),
    ('Nominal', ('Nominal', 'PP',)),
]
Words = ['book', 'that', 'flight']

# constants
DOT = '•'

# main
def parse(words=Words, grammar=Grammar):
    chart = [[] for i in range(0, len(words)+1)]
    enqueue(('γ', (DOT, 'S'), (0, 0)), chart, 0, '')
    for i in range(0, len(words)+1):
        for state in chart[i]:
            if type(state[1]) == tuple:
                if not is_complete(state) and not is_next_cat_is_pos(state, grammar):
                    predictor(state, chart, grammar)
                elif not is_complete(state) and is_next_cat_is_pos(state, grammar):
                    scanner(state, chart, words, grammar)
                else:
                    completer(state, chart)
    return chart

def predictor(state, chart, grammar):
    b = get_after_dot(state)
    i, j = state[2]
    for rule in get_rules_for(b, grammar):
        state_new = (b, (DOT,)+rule, (j, j))
        enqueue(state_new, chart, j, 'PREDICTOR: ')

def scanner(state, chart, words, grammar):
    b = get_after_dot(state)
    i, j = state[2]
    if j < len(words) and words[j] in get_rules_for(b, grammar)[0][0]:
        state_new = (b, words[j], (j, j+1))
        enqueue(state_new, chart, j+1, 'SCANNER: ')
        state_new = (state[0], forward_dot(state), (j, j+1))
        enqueue(state_new, chart, j+1, ' '*9)

def completer(state, chart):
    j, k = state[2]
    for entry in chart[j]:
        if not is_complete(entry) and get_after_dot(entry) == state[0]:
            if entry[0] != 'γ':
                state_new = (entry[0], forward_dot(entry), (entry[-1][0], k))
                enqueue(state_new, chart, k, 'COMPLETER: ')

def enqueue(state, chart, i, act):
    is_in_chart = False
    for entry in chart[i]:
        if (entry[0], entry[1]) == (state[0], state[1]):
            is_in_chart = True
            break
    if not is_in_chart:
        chart[i].append(state)
        log(act, chart, i)

def is_complete(state):
    return state[1][-1] == DOT

def is_next_cat_is_pos(state, grammar):
    next_cat = get_after_dot(state)
    for rule in grammar:
        if rule[0] == next_cat and type(rule[1][0]) == list:
            return True
    return False

def get_rules_for(b, grammar):
    rules = []
    for rule in grammar:
        if rule[0] == b:
            rules.append(rule[1])
    return rules

def get_after_dot(state):
    for i in range(0, len(state[1])):
        if state[1][i] == DOT:
            return state[1][i+1]

def forward_dot(state):
    new = []
    i = 0
    while i < len(state[1]):
        if state[1][i] == DOT:
            new.append(state[1][i+1])
            new.append(DOT)
            i += 1
        else:
            new.append(state[1][i])
        i += 1
    return tuple(new)

def log(act, chart, i):
    def tuple2str(t):
        res = ''
        for x in t:
            res += ' '+x
        return res
    state = chart[i][-1]
    print '{}{} ->{}, [{}, {}]; chart[{}]'.format(
        act,
        state[0], tuple2str(state[1]) if type(state[1]) == tuple else ' '+state[1],
        state[2][0], state[2][1], i)

if __name__ == '__main__':
    parse()
